module.exports = {
  local: 'http://localhost:3000/',
  prod: 'https://deploy-test-prod.herokuapp.com/',
  qa: 'https://deploy-test-qa.herokuapp.com/'
}
